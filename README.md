# Microservices 
This repository is a demo for microservices applications using symfony. 
It consists of 2 applications (microservices) and the rabbitmq middleware : 
- app1 : listens and consumes app2 messages from rabbitmq queue
- app2 : send messages to rabbitmq 

Each application is containerized using docker

# Requirements
Install docker on your computer

# Setup
Clone this repository into a "Microservices" directory : 
```
git clone --recurse-submodules git@gitlab.com:tiatony/microservices.git
```

Go into the Microservices directory and clone app1 and app2 repositories.
Your directory and file tree should now look as follows:
```
Microservices
├── app1
│   └── Dockerfile
|       (...) # Symfony App Structure
├── app2
│   └── Dockerfile
|       (...) # Symfony App Structure
└── docker-compose.yml
```

Go to Microservices directory and start containers : 
```
docker-compose up --build -d
```

Install dependencies for app1 and app2 : 
```
docker exec -it app1 composer install
docker exec -it app2 composer install
```

## Execute



To send a message, from the container app2, execute
```
docker exec -it app2 bin/console app:send
```

To consume messages, from the container app1, execute
```
docker exec -it app1 bin/console messenger:consume -vv external_messages
```
You should see the message :
```
APP1: {STATUS_UPDATE} - Worker X assigned to Y
```

Source : https://thecodest.co/blog/microservices-communication-in-symfony-part-i-1/ 